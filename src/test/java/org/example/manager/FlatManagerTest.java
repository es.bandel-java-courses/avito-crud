package org.example.manager;

import org.example.data.Flat;
import org.example.data.FlatSearch;
import org.example.exception.FlatIsNotFoundException;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class FlatManagerTest {
    static final FlatManager flatManager = new FlatManager();
    static final Flat flatFirst = new Flat(0, 4, false, false, 2000000, 65, true, false, 3, 6);
    static final Flat flatSecond = new Flat(0, 4, false, false, 3500000, 25, false, false, 3, 10);
    static final Flat flatThird = new Flat(0, 3, false, false, 3500000, 25, true, false, 4, 10);
    static final Flat flatFourth = new Flat(0, 3, false, false, 3500000, 50, true, false, 3, 8);
    static final Flat flatFifth = new Flat(0, 4, false, false, 2000000, 65, true, false, 1, 6);
    static final Flat flatSixth = new Flat(0, 4, false, false, 2000000, 78, true, false, 3, 6);

    @Test
    @Order(1)
    void shouldCreate() {
        List<Flat> expectedAll = new ArrayList<>();
        List<Flat> actualAll = flatManager.getAll();
        assertEquals(expectedAll, actualAll);
    }

    @Test
    @Order(2)
    void shouldAddSingle() {
        int notExpectedId = 0;
        Flat actualCreate = flatManager.create(flatFirst);
        assertNotEquals(notExpectedId, actualCreate.getId());

        List<Flat> expectedAll = new ArrayList(Arrays.asList(flatFirst));
        List<Flat> actualAll = flatManager.getAll();
        assertEquals(expectedAll, actualAll);

        int actualById = flatManager.getIndexById(actualCreate.getId());
        assertEquals(0, actualById);
    }


    @Test
    @Order(3)
    void shouldAddMultiple() {
        int notExpectedId = 0;
        Flat actualCreate = flatManager.create(flatSecond);
        assertNotEquals(notExpectedId, actualCreate.getId());

        List<Flat> expectedAll = new ArrayList(Arrays.asList(flatFirst, flatSecond));
        List<Flat> actualAll = flatManager.getAll();
        assertEquals(expectedAll, actualAll);

        int actualById = flatManager.getIndexById(actualCreate.getId());
        assertEquals(1, actualById);
    }

    @Test
    @Order(4)
    void shouldGetCount() {
        int expected = 2;
        int actual = flatManager.getCount();
        assertEquals(expected, actual);
    }

    @Test
    @Order(5)
    void shouldSearchIfNotStudio() {
        FlatSearch flatSearch = new FlatSearch(0, true, false, 1000000, 6000000, 1, 100, false, false, 1, 9, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(6)
    void shouldSearchIfNotFreePlanned() {
        FlatSearch flatSearch = new FlatSearch(0, false, true, 1000000, 6000000, 1, 100, false, false, 1, 9, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();
        assertEquals(expected, actual);
    }

    @Test
    @Order(7)
    void shouldSearchIfPriceMoreThanMaxPrice() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 3000000, 1, 100, false, false, 1, 9, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(8)
    void shouldSearchIfPriceLessThanMinPrice() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 5000000, 6000000, 1, 100, false, false, 1, 9, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(9)
    void shouldSearchIfMoreThanMaxArea() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 1, 20, false, false, 1, 9, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(10)
    void shouldSearchIfLessThanMinArea() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 30, 60, false, false, 1, 9, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(11)
    void shouldSearchIfNoBalcony() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 20, 60, true, false, 1, 9, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(12)
    void shouldSearchIfNoLogia() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 20, 60, false, true, 1, 9, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(13)
    void shouldSearchIfFloorMoreThanMax() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 20, 60, false, false, 1, 2, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(14)
    void shouldSearchIfFloorLessThanMin() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 20, 60, false, false, 5, 7, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(15)
    void shouldSearchIfFirstFloor() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 20, 70, true, false, 1, 7, true, true, 1, 11);
        flatFirst.setFloor(1);
        flatSecond.setFloor(1);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();
        assertEquals(expected, actual);
    }

    @Test
    @Order(16)
    void shouldSearchFirstFloor() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 20, 70, true, false, 1, 7, false, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();
        expected.add(flatFirst);
        assertEquals(expected, actual);
    }

    @Test
    @Order(17)
    void shouldSearchIfLastFloor() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 20, 70, true, false, 1, 11, true, true, 1, 11);
        flatFirst.setFloor(6);
        flatSecond.setFloor(10);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();
        assertEquals(expected, actual);
    }

    @Test
    @Order(18)
    void shouldSearchLastFloor() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 20, 70, true, false, 1, 11, true, false, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();
        expected.add(flatFirst);
        flatFirst.setFloor(4);
        flatSecond.setFloor(4);
        assertEquals(expected, actual);
    }


    @Test
    @Order(19)
    void shouldSearchIfFloorInHouseMoreThanMax() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 20, 60, false, false, 1, 7, true, true, 1, 9);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(20)
    void shouldSearchIfFloorInHouseLessThanMin() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 20, 60, false, false, 1, 7, true, true, 12, 15);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(21)
    void shouldSearchIfRoomAmountNotEquals() {
        FlatSearch flatSearch = new FlatSearch(3, false, false, 1000000, 6000000, 20, 60, false, false, 1, 9, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();

        assertEquals(expected, actual);
    }

    @Test
    @Order(22)
    void shouldMatchSomeFlats() {
        FlatSearch flatSearch = new FlatSearch(3, false, false, 1000000, 6000000, 20, 80, true, false, 1, 10, true, true, 1, 11);
        flatManager.create(flatThird);
        flatManager.create(flatFourth);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList(Arrays.asList(flatThird, flatFourth));
        assertEquals(expected, actual);
    }

    @Test
    @Order(23)
    void shouldRemoveByIdNotExists() {
        boolean actualRemove = flatManager.removeById(999);
        assertFalse(actualRemove);
    }

    @Test
    @Order(24)
    void shouldRemoveByIdExistent() {
        boolean actualRemove = flatManager.removeById(flatSecond.getId());
        assertTrue(actualRemove);
    }

    @Test
    @Order(25)
    void shouldRemoveByIdIfRemoved() {
        boolean actualRemove = flatManager.removeById(2);
        assertFalse(actualRemove);
    }

    @Test
    @Order(26)
    void shouldSearchIfRemoved() {
        FlatSearch flatSearch = new FlatSearch(4, false, false, 1000000, 6000000, 20, 30, false, false, 1, 9, true, true, 1, 11);
        List<Flat> actual = flatManager.search(flatSearch);
        List<Flat> expected = new ArrayList<>();
        assertEquals(expected, actual);
    }

    @Test
    @Order(27)
    void shouldGetByIdIfExist() {
        Flat actual = flatManager.getById(4);
        Flat expected = flatFourth;
        assertEquals(expected, actual);
    }

    @Test
    @Order(28)
    void shouldGetByIdIfNotExist() {
        assertThrows(FlatIsNotFoundException.class, () -> flatManager.getById(55));
    }

    @Test
    @Order(29)
    void shouldGetByIdIfRemoved() {
        assertThrows(FlatIsNotFoundException.class, () -> flatManager.getById(2));
    }

    @Test
    @Order(30)
    void shouldUpdateIfNotExists() {
        assertThrows(FlatIsNotFoundException.class, () -> flatManager.update(flatSecond));
    }

    @Test
    @Order(31)
    void shouldUpdateIfExists() {
        flatManager.create(flatSixth);
        flatSixth.setId(flatFirst.getId());
        flatManager.update(flatSixth);
        int expected = 78;
        int actual = flatSixth.getArea();
        assertEquals(expected, actual);
    }

    @Test
    @Order(32)
    void shouldGetCountIfWithRemoved() {
        int expected = 4;
        int actual = flatManager.getCount();
        assertEquals(expected, actual);
    }

    @Test
    @Order(33)
    void shouldGetAllWithoutRemoved() {
        List<Flat> expectedAll = new ArrayList(Arrays.asList(flatSixth, flatThird, flatFourth, flatSixth));
        List<Flat> actualAll = flatManager.getAll();
        assertEquals(expectedAll, actualAll);
    }

}