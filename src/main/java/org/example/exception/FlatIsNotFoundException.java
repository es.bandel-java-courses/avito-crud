package org.example.exception;

public class FlatIsNotFoundException extends RuntimeException {
    public FlatIsNotFoundException() {
        super();
    }

    public FlatIsNotFoundException(String message) {
        super(message);
    }

    public FlatIsNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FlatIsNotFoundException(Throwable cause) {
        super(cause);
    }

    protected FlatIsNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
