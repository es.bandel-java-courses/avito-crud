package org.example.data;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data


public class FlatSearch {
    private int roomsAmount;
    private boolean isStudio;
    private boolean isFreePlanned;
    private int priceMin;
    private int priceMax;
    private int areaMin;
    private int areaMax;
    private boolean hasBalcony;
    private boolean hasLogia;
    private int floorMin;
    private int floorMax;
    private boolean floorNotFirst;
    private boolean floorNotLast;
    private int floorsInHouseMin;
    private int floorsInHouseMax;

}
