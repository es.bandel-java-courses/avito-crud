package org.example.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data

public class Flat {
    private long id;
    private int roomsAmount;
    private boolean isStudio;
    private boolean isFreePlanned;
    private int price;
    private int area;
    private boolean hasBalcony;
    private boolean hasLogia;
    private int floor;
    private int floorsInHouse;
    private boolean removed;

    public Flat(long id, int roomsAmount, boolean isStudio, boolean isFreePlanned, int price,
                int area, boolean hasBalcony, boolean hasLogia, int floor, int floorsInHouse) {
        this(id, roomsAmount, isStudio, isFreePlanned, price, area, hasBalcony, hasLogia, floor, floorsInHouse, false);
    }
}


