package org.example.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.data.Flat;
import org.example.data.FlatSearch;
import org.example.exception.FlatIsNotFoundException;

import java.util.ArrayList;


import java.util.List;

@Slf4j
public class FlatManager {
    private final List<Flat> flats = new ArrayList<>(50);
    private long nextId = 1;

    public List<Flat> getAll() {
        final List<Flat> flatsCopy = new ArrayList<>(50);
        for (final Flat flat : flats) {
            if (!flat.isRemoved()) {
                flatsCopy.add(flat);
            }
        }
        return flatsCopy;
    }

    public Flat getById(final long id) {
        for (final Flat flat : flats) {
            if ((flat.getId() == id)&&(!flat.isRemoved())) {
                return flat;
            }
        }
        throw new FlatIsNotFoundException("Flat with id " + id + " is not found");
    }

    public Flat create(final Flat flat) {
        log.debug("Created flat, Id: {}", nextId);
        flat.setId(nextId);
        flats.add(flat);
        nextId++;
        return flat;
    }

    public int getIndexById(final long id) {
        for (int index = 0; index < flats.size(); index++) {
            final Flat currentFlat = flats.get(index);
            if (currentFlat.getId() == id&&!currentFlat.isRemoved()) {
                return index;
            }
        }
        return -1;
    }

    public Flat update(final Flat flat) {
        final int index = getIndexById(flat.getId());
        if (index == -1) {
            throw new FlatIsNotFoundException("Impossible to update flat " + flat.getId() + " because it is not found");
        }
        flats.set(index, flat);
        log.debug("Flat with id {} is updated", flat.getId());
        return flat;
    }

    public boolean removeById(final long id) {
        for (final Flat currentFlat : flats) {
            if ((currentFlat.getId() == id)&&(!currentFlat.isRemoved())) {
                currentFlat.setRemoved(true);
                log.debug("Flat with id {} is removed", id);
                return true;
            }
        }
        return false;
    }

    public int getCount() {
        int flatsSize=flats.size();
        for (final Flat flat : flats) {
            if (flat.isRemoved()) {
                flatsSize--;
            }
        }
        log.debug("Amount of flats is {}", flatsSize);
        return flatsSize;
    }

    public List<Flat> search(FlatSearch flatSearch) {
        final List<Flat> flatsAfterFilters = new ArrayList<>(50);
        for (final Flat flat : flats) {
            if (comparingFlats(flatSearch, flat)) {
                flatsAfterFilters.add(flat);
            }
        }
        return flatsAfterFilters;
    }

    public boolean comparingFlats(FlatSearch flatSearch, Flat flat) {
        if (flat.isRemoved()){
            return false;
        }
        if (flat.isStudio() != flatSearch.isStudio()) {
            return false;
        }
        if (flat.isFreePlanned() != flatSearch.isFreePlanned()) {
            return false;
        }
        if (flat.getRoomsAmount() != flatSearch.getRoomsAmount()) {
            return false;
        }
        if (flat.getPrice() > flatSearch.getPriceMax()) {
            return false;
        }
        if (flat.getPrice() < flatSearch.getPriceMin()) {
            return false;
        }
        if (flat.getArea() > flatSearch.getAreaMax()) {
            return false;
        }
        if (flat.getArea() < flatSearch.getAreaMin()) {
            return false;
        }
        if (flat.isHasBalcony() != flatSearch.isHasBalcony()) {
            return false;
        }
        if (flat.isHasLogia() != flatSearch.isHasLogia()) {
            return false;
        }
        if (flat.getFloor() > flatSearch.getFloorMax()) {
            return false;
        }
        if (flat.getFloor() < flatSearch.getFloorMin()) {
            return false;
        }
        if ((flatSearch.isFloorNotFirst()) && (flat.getFloor() == 1)) {
            return false;
        }
        if ((flatSearch.isFloorNotLast()) && (flat.getFloor() == flat.getFloorsInHouse())) {
            return false;
        }
        if (flat.getFloorsInHouse() > flatSearch.getFloorsInHouseMax()) {
            return false;
        }
        if (flat.getFloorsInHouse() < flatSearch.getFloorsInHouseMin()) {
            return false;
        }
        return true;
    }

}
